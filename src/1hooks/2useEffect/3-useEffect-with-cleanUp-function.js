import react, { useState, useEffect } from 'react'
// by default runs after every re-render
// second parameter
// cleanup function
// hook must be included inside the component
// we cannot render hooks conditionally but we can use conditions inside hooks

const UseEffectWithCleanUpFunction = () =>{
    const [size, setSize] = useState(window.innerWidth)
    console.log(size);

    const checkSize = () =>{
        setSize(window.innerWidth)
    }

    useEffect(() => {
        console.log('inside use effect and it running')
        //addEventListern function accept two parameter, one is event and one is call back function
           window.addEventListener('resize',checkSize)

           return ()=>{
               console.log('CleanUp function running')
               window.removeEventListener('resize', checkSize)
           }
    })
    console.log('outside use effect')
    return <>
        <h2>Use Effect Conditions with clean up function and event listeners</h2>
        <p>see the result in console.</p>
        <h1>Window size is</h1>
        <h2>{size} PX</h2>
        <button className='btn'>Click me</button>
    </>
}


export default UseEffectWithCleanUpFunction