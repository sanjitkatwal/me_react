import react, { useState, useEffect } from 'react'
// by default runs after every re-render
// second parameter
// cleanup function
// hook must be included inside the component

const UseEffectBasics = () =>{
    const [value, setValue] = useState(0)

    useEffect(() => {
        console.log('inside use effect')
    })
    console.log('outside use effect')
    return <>
        <h2>Use Effect Basics</h2>
        <h1>{value}</h1>
        <button className='btn' onClick= {()=> setValue(value+1)}>Click me</button>
    </>
}


export default UseEffectBasics