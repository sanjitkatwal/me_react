import react, { useState, useEffect } from 'react'


const GithubApiImplement = () =>{
    const url = 'https://api.github.com/users'

    const [users, setUsers] = useState([])

    const getUsers = async () => {
        const response = await fetch(url)
        const users = await response.json()
        console.log(users)

         setUsers(users)
    }

    useEffect(() =>{
        getUsers()
    }, [])

    return(
        <>
        <h2>Git hub Users api implemntations.</h2>
        <ul className = 'users'>
        {users.map((user) =>{
            const {id, login, html_url, avatar_url} = user
            return(
                <li key={id}>
                    <img src={avatar_url} alt={login} />
                    <div>
                    <h4>Name: {login}</h4>
                    <a href={html_url}>Github Profile</a>
                    </div>
                </li>
            )
        })}
        </ul>
        
        </>
    )
}


export default GithubApiImplement