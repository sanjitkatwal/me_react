import react, { useState, useEffect } from 'react'
// by default runs after every re-render
// second parameter
// cleanup function
// hook must be included inside the component
// we cannot render hooks conditionally but we can use conditions inside hooks

const UseEffectConditions = () =>{
    const [value, setValue] = useState(0)

    useEffect(() => {
        console.log('inside use effect')
        if(value >=1){
            document.title = `New Message (${value})`
        }     
      
    }, [value])
    console.log('outside use effect')
    return <>
        <h2>Use Effect Conditions</h2>
        <h1>{value}</h1>
        <button className='btn' onClick= {()=> setValue(value+1)}>Click me</button>
    </>
}


export default UseEffectConditions