import React, { useState } from 'react'
import {data} from '../data'
import List from './list'


const PropDrilling = ()=> {

    const [people, setPeople] = useState(data)
    return(
        <div>
            <h1>Prop Drilling</h1>
            <hr />

            <section>
            
                <List people={people} />
            </section>
        </div>
    )
}


export default PropDrilling