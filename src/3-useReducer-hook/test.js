import { useReducer } from "react";

const initialState = 0;

const reducer = (state, action) => {
    
    if (action.type === "INCREMENT"){
        return state +1
    }

    if (action.type === "DECREMENT"){
        return state -1
    }
    

    return state

}

const Test = () => {
    const [state, dispatch] = useReducer(reducer, initialState)

    return(
        <>
        <p>{state}</p>
        <div>
            <button onClick={() =>dispatch({type: "INCREMENT"})}>Inc</button>
            <button onClick={() =>dispatch({type: "DECREMENT"})}>Inc</button>
        
        </div>
        </>
    )
}


export default Test