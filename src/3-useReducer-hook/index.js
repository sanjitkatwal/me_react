import React, { useState, useReducer } from 'react'
import Modal from './Modal'
import { data } from '../data'



    const initialState = {
        people: [],
        isModalOpen:false,
        modalContent: 'Hello'
    }

    const reducer = (state, action) => {
        if (action.type === 'ADD_ITEM'){
            const newPeople = [...state.people, action.payload]
            return {...state, people: newPeople, isModalOpen: true, modalContent: 'Item Added'}
        }

        if (action.type === 'CLOSE_MODAL'){
            return {...state, isModalOpen:false}
        }
        if (action.type === 'NO_VALUE'){
            const newPeople = [...state.people, action.payload]
            return {...state, isModalOpen: true, modalContent:'Please Enter Valid Input.'}
        }


        throw new Error('No any matching dispatch found')
        
        //console.log(action)
    }
const Index = () => {
    
const [name, setName] = useState('')
// const [people, setPeople] = useState(data)
// const [showModal, setShowModal] = useState(false)
    
    const [state, dispatch] = useReducer(reducer, initialState)

    const handleSubmit = (e) => {
        e.preventDefault()
        if(name){
            // setShowModal(true)
            // setPeople([...people, {id: new Date().getTime().toString() }])
            // setName('')
            const newItem = {id: new Date().getTime().toString(), name }
            // dispatch({type: 'ADD_ITEM', payload: name })
            dispatch({type: 'ADD_ITEM', payload: newItem })
            setName('')

        }else{
           dispatch({type: 'NO_VALUE'})
        }
    }

    const closeModal = () => {
        dispatch({type: 'CLOSE_MODAL'})
    }
    return(
    <div>
        <h2>useReducer</h2>
        <hr />
        {state.isModalOpen && <Modal modalContent = {state.modalContent} closeModal ={closeModal}  />}
        <form onSubmit = {handleSubmit} className='form'>
            <div>
                <input type='text' value={name} onChange={(e) => {setName(e.target.value)}} />
                
            </div>
            <button type='submit'>Submit</button>
        </form>
        { state.people.map((person) => {
            return(
                <div key={person.id} className='item'>
                <p>{person.name}</p>
                <button>Remove</button>
                </div>
                
            )
        })}
        {/* { people.map((person) => {
            return(
                <div key={person.id}>
                <p>{person.name}</p>
                </div>
            )
        })} */}
    </div>
    )
}



export default Index