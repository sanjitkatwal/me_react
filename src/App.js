// import UseStateBasics from './1hooks/1-useState/1-useState-Basic'
//  import UseStateArray from './1hooks/1-useState/2-useState-Array'
// import UseStateObject from './1hooks/1-useState/3-useState-Object'
// import UseStateFunction from './1hooks/1-useState/4-useState-Function'
// import UseEffect from './1hooks/2useEffect/1-useEffect-basics' 
// import UseEffectConditions from './1hooks/2useEffect/2-useEffect-condition' 
// import UseEffectWithCleanUpFunction from './1hooks/2useEffect/3-useEffect-with-cleanUp-function' 
// import GithubApiImplement from './1hooks/2useEffect/4-githubapiImplement'
// import MultipleReturns from './1hooks/3-multiple-returns/1-multiple-return'
// import FormBasics from './2-react-form/1-form-basics'
// import FormAdvanced from './2-react-form/2-form-advanced'
// import UseRefHook from './2-react-form/3-useRef-hook'
import UseRefHook from './3-useReducer-hook'
// import Test from './3-useReducer-hook/test'
import PropDrilling from './4-prop-drilling/1-prop-drilling'



function App() {
  return (
    <div className='container'>
      {/* <UseStateBasics /> */}
      {/* <UseStateArray /> */}
      {/* <UseStateObject/> */}
      {/* <UseStateFunction /> */}
      {/* <UseEffect /> */}
      {/* <UseEffectConditions /> */}
      {/* <UseEffectWithCleanUpFunction /> */}
      {/* <GithubApiImplement /> */}
      {/* <MultipleReturns /> */}
      {/* <FormBasics /> */}
      {/* <FormAdvanced /> */}
      {/* <UseRefHook /> */}

      <UseRefHook />
      {/* <Test /> */}
      {/* <PropDrilling /> */}
    </div>
  )
}

export default App
