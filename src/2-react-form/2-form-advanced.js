import React, { useState } from "react"



const FormAdvanced = () => {

    // const [firstName, setFirstName] = useState('')
    // const [email, setEmail] = useState('')
    // const [age, setAge] = useState('')
    const [person, setPerson] = useState({ firstName:'', email:'', age:'' })
    const [people, setPeople]  = useState([])

    // const handleSubmit = (e) => {
    //     e.preventDefault()
    //     const person = {id: new Date().getTime(),  firstName, email}
    //     console.log(firstName, email)
    //     if(firstName && email){
    //         setPeople((previousState) => {
    //             return[...previousState, person]
    //         })
    //         setFirstName('')
    //         setEmail('')
    //     }else{
    //         console.log('Your value is empty!')
    //     }
    // }

    const handleSubmit = (e) =>{
        e.preventDefault()
        
        if(person.firstName && person.email && person.age){
            const newPerson = {...person, id: new Date().getTime().toString()}
            setPeople([...people, newPerson])

            setPerson({ firstName:'', email:'', age:'' })
        }
    }

    const handleChange = (e) =>{
        const name = e.target.name
        const value = e.target.value
        setPerson({...person, [name]:value})
        console.log(name, value)
    }
    return ( 
    <>
        <h2> React Form Basics </h2> 
        <article>
            <form className='form'>
                <div className='form-control'>
                    <label htmlFor='firstName' > Enter Name: </label> 
                    <input type='text' id='firstName'name='firstName' value={person.firstName} onChange={handleChange} />
                </div>

                <div className='form-control'>
                    <label htmlFor='email' > Enter Name: </label> 
                    <input type='text' id='email'name='email' value={person.email} onChange={handleChange} />
                </div>

                <div className='form-control'>
                    <label htmlFor='email' > Enter Age: </label> 
                    <input type='text' id='age'name='age' value={person.age} onChange={handleChange} />
                </div>

                <button type='submit' onClick={handleSubmit}> Add Person </button>

            </form> 
            {
                people.map((person) => {
                    const {id, firstName, email,age} = person

                    return <div className='item' key={id}>
                        <h4>{firstName}</h4>
                        <h4>{email}</h4>
                        <h4>{age}</h4>
                    </div>
                })
            }
        </article> 
    </>
        )
}


export default FormAdvanced