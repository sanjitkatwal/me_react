import React, { useState } from "react"



const FormBasics = () => {

    const [firstName, setFirstName] = useState('')
    const [email, setEmail] = useState('')
    const [people, setPeople]  = useState([])

    const handleSubmit = (e) => {
        e.preventDefault()
        const person = {id: new Date().getTime(),  firstName, email}
        console.log(firstName, email)
        if(firstName && email){
            setPeople((previousState) => {
                return[...previousState, person]
            })
            setFirstName('')
            setEmail('')
        }else{
            console.log('Your value is empty!')
        }
    }

    return ( 
    <>
        <h2> React Form Basics </h2> 
        <article>
            <form className='form'onSubmit={handleSubmit} >
                <div className='form-control'>
                    <label htmlFor='firstName' > Enter Name: </label> 
                    <input type='text' id='firstName'name='firstName' value={firstName} onChange={(e) => setFirstName(e.target.value)} />
                </div>

                <div className='form-control'>
                    <label htmlFor='email' > Enter Name: </label> 
                    <input type='text' id='email'name='email' value={email} onChange={(e) => setEmail(e.target.value)} />
                </div>

                <button type='submit'> Add Person </button>

            </form> 
            {
                people.map((person) => {
                    const {id, firstName, email} = person

                    return <div className='item' key={id}>
                        <h4>{firstName}</h4>
                        <h4>{email}</h4>
                    </div>
                })
            }
        </article> 
    </>
        )
}


export default FormBasics