import React, { useRef } from 'react'


const UseRefHook = () => {
    const refContainer = useRef(null)
    console.log(refContainer)
    const handleSubmit = (e) => {
        e.preventDefault()
        console.log(refContainer.current.value)
    }
    return(
        <div>
            <h1>Use Ref Hooks</h1>
            <form className='form' onSubmit= {handleSubmit}>
                <div>
                    <input type='text' />
                    <button type='submit'>Submit</button>
                </div>
            </form>
        </div>
    )
}


export default UseRefHook